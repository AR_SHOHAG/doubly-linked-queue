#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>

  struct queues {
        int data;
        struct queues *prev, *next;
  };

  struct queues *head = NULL, *tail = NULL;

  struct queues * createQueues(int data) {
        struct queues *newqueues = (struct queues *)malloc(sizeof (struct queues));
        newqueues->data = data;
        newqueues->next = newqueues->prev = NULL;
        return (newqueues);
  }

   /*
    * create sentinel(dummy head & tail) that
    * helps us to do insertion and deletion
    * operation at front and rear so easily.  And
    * these dummy head and tail wont get deleted
    * till the end of execution of this program
    */

  void createSentinels() {
        head = createQueues(0);
        tail = createQueues(0);
        head->next = tail;
        tail->prev = head;
  }

  /* insertion at the front of the queue */
  void insert_front(int data) {
        struct queues *newqueues, *temp;
        newqueues = createQueues(data);
        temp = head->next;
        head->next = newqueues;
        newqueues->prev = head;
        newqueues->next = temp;
        temp->prev = newqueues;
  }

  /*insertion at the rear of the queue */
  void insert_rear(int data) {
        struct queues *newqueues, *temp;
        newqueues = createQueues(data);
        temp = tail->prev;
        tail->prev = newqueues;
        newqueues->next = tail;
        newqueues->prev = temp;
        temp->next = newqueues;
  }

  /* deletion at the front of the queue */
  void delete_front() {
        struct queues *temp;
        if (head->next == tail) {
                printf("Queue is empty\n");
        } else {
                temp = head->next;
                head->next = temp->next;
                temp->next->prev = head;
                free(temp);
        }
        return;
  }


  /* deletion at the rear of the queue */

  void delete_rear()  {
        struct queues *temp;
        if (tail->prev == head) {
                printf("Queue is empty\n");
        } else {
                temp = tail->prev;
                tail->prev = temp->prev;
                temp->prev->next = tail;
                free(temp);
        }
        return;
  }

  /* display elements present in the queue */
  void display() {
        struct queues *temp;

        if (head->next == tail) {
                printf("Queue is empty\n");
                return;
        }

        temp = head->next;
        while (temp != tail) {
                printf("%-3d", temp->data);
                temp = temp->next;
        }
        printf("\n");
  }

  int main() {
        int data, ch;
        createSentinels();
        while (1) {
                printf("1. Insert element to queue front\n2. Insert element to queue at rear\n");
                printf("3. Delete element from queue at front\n4. Delete element from queue at rear\n");
                printf("5. Display\n6. Exit\n");
                printf("Enter your choice:");
                scanf("%d", &ch);
                switch (ch) {
                        case 1:
                                printf("Enter the data to insert:");
                                scanf("%d", &data);
                                insert_front(data);
                                break;

                        case 2:
                                printf("Enter ur data to insert:");
                                scanf("%d", &data);
                                insert_rear(data);
                                break;

                        case 3:
                                delete_front();
                                break;

                        case 4:
                                delete_rear();
                                break;

                        case 5:
                                display();
                                break;

                        case 6:
                                exit(0);

                        default:
                                printf("Pls. enter correct option\n");
                                break;
                }
        }
        return 0;
  }

